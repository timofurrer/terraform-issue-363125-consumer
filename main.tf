
terraform {
  backend "http" {}
}

module "my_module_name" {
  source = "gitlab.com/timofurrer/terraform-issue-363125-module/local"
  version = "2.0.0"

  text = "Hello World"
  filename = "hello"
}

output "filesize_in_bytes" {
  value = module.my_module_name.bytes
}
